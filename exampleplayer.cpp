#include <iostream>
#include "exampleplayer.h"
#include <vector>
using namespace std;

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
	board = new Board();
	_side = side;
    
    //initialize weights for each cell
    initWeights();
    
}

void ExamplePlayer::initWeights(){
	//corner points
    int corner[4][2] = {{0,0},{0,7},{7,0},{7,7}};
	//points next to corner
    int nexttocorner[12][2] = {{1,0},{0,1},{1,7},{0,6},{6,0},{7,1},{6,7},{7,6},{1,1},{1,6},{6,1},{6,6}};

	// initialize the weight table
    for(int i=0; i<8; i++)
        for(int j=0; j<8; j++)
            weightTable[i][j] = 0;
    
	// goes through each cell in the game
    for(int i=0; i< 8; i++){
        for(int j=0; j < 8; j++){
            int found = 0;
	
			// first, check if it's a corner point. if it is, set weight to 2
            for(int k = 0; k < 4; k++){
                if(i==corner[k][0] && j==corner[k][1]){
                    weightTable[i][j] = 2;
                    found = 1;
                }
            }
			
			// if not a corner point, check if it's a next-to-corner point
			// if so, set weight to -2
            if(found == 0){
                for(int k = 0; k < 12; k++){
                    if(i == nexttocorner[k][0] && j == nexttocorner[k][1]){
                        weightTable[i][j] = -2;
                        found = 1;
                    }                
                }
            }
	
			// if not any, check if it's an edge point
			// if so, set weight to 2, if not, set it to 1.
            if(found == 0){
                if(i == 0 || j == 0 || i == 7 || j == 7){
                    weightTable[i][j] = 2;
                }
                else{
                    weightTable[i][j] = 1;
                }
            }
        }
    }
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
	delete(board);
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    
    //update opponents move:
    Side otherside = (_side == BLACK) ? WHITE : BLACK;
    board->doMove(opponentsMove, otherside);

    // create a vector of scores for each move
	std::vector<int> scores (64);
	for(int i=0; i < 8*8; i++)
		scores[i] = 0;

    // if you cannot make a move, return null
	if(! board->hasMoves(_side) ){
		return NULL;
    }

    // otherwise, check the score for each move
	for(int i=0; i < 8; i++){
		for(int j=0; j<8; j++){
            
			Move *move = new Move(i, j);
            
            // If the move is valid, evaluate its score
			if(board->checkMove(move, _side)){
                // check how many white pieces you can convert
                scores[i*8+j] += board->evalMove(move, _side);
                // give priority to corner and edges, and less to next-to-corner edges
				// by looking up the weight in the weightTable.
                int weight = weightTable[i][j];
                scores[i*8+j] *= weight;
                
			}
			else{
				scores[i*8+j] = -10000000;
			}
			delete(move);
		}	
	}

	// Goes and find the move with the best heuristics
	int max_index = 0;
	for(int i=0; i < 8*8; i++){
		if (scores[i] > scores[max_index])
			max_index = i;	
	}
	
	// create the move
	int x = (int) max_index/8;
	int y = (int) max_index%8;
    std::cerr << x << " " << y << endl;
	Move *bestmove = new Move(x,y);
    
    //update own move:
    board->doMove(bestmove, _side);
    return bestmove;
}
    
