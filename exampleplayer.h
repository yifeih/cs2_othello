#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>
using namespace std;

class ExamplePlayer {

private:
	Board *board;
	Side _side;
    int weightTable[8][8]; // list of weights for each cell for heuristics
    
    void initWeights();

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    Move *doMove(Move *opponentsMove, int msLeft);
};

#endif
